package candidate.plaformscience.acmeshipmentrouter.util

class SuitabilityScoreCalculator {

    fun isLengthEven(string: String) : Boolean {
        return string.length % 2 == 0
    }

    fun findNumberOfVowels(string: String) : Int {
        val s = string.toLowerCase()

        var vowelCount = 0

        for (i in s.indices) {
            when(s[i]){
                'a', 'e', 'i', 'o', 'u' -> vowelCount++
            }
        }

        return vowelCount
    }

    fun findNumberOfConsonants(string: String) : Int {
        val s = string.toLowerCase()

        var consonantsCount = 0

        for (i in s.indices) {
            when(s[i]){
                in 'a'..'z' -> consonantsCount++
            }
        }

        return consonantsCount
    }

    fun getSSValueByAlgorithmRule1(name: String) : Double {
        return findNumberOfVowels(name) * 1.5
    }

    fun getSSValueByAlgorithmRule2(name: String) : Double {
        return findNumberOfConsonants(name) * 1.0
    }

    fun scaleSSValueByAlgorithmRule3(value: Double, streetName: String, driverName: String) : Double {
        val gcf = GCF(streetName.length, driverName.length)
        return if (gcf > 1) value * 1.5 else value
    }

    fun GCF(a: Int, b: Int): Int {
        return if (b == 0) a else GCF(b, a % b)
    }
}