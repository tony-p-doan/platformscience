package candidate.plaformscience.acmeshipmentrouter.util

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import candidate.plaformscience.acmeshipmentrouter.R
import candidate.plaformscience.acmeshipmentrouter.model.RouteData

class DriversAdapter(private val mList: List<RouteData>, private val context: Context) : RecyclerView.Adapter<DriversAdapter.ViewHolder>() {
    var onItemClick: ((RouteData) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.driver_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val itemsViewModel = mList[position]

        holder.driverItem.setText(itemsViewModel.driver.name)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    inner class ViewHolder(ItemView: View) : RecyclerView.ViewHolder(ItemView) {
        val driverItem: TextView = itemView.findViewById(R.id.tvDriverName)

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(mList[adapterPosition])
            }
        }
    }
}