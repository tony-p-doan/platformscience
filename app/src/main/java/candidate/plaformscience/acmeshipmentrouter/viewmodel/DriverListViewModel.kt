package candidate.plaformscience.acmeshipmentrouter.viewmodel

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import candidate.plaformscience.acmeshipmentrouter.model.RouteData
import candidate.plaformscience.acmeshipmentrouter.model.SuitabilityScoreModel


class DriverListViewModel : ViewModel() {
    var driversLiveData: MutableLiveData<List<RouteData>> = MutableLiveData()

    lateinit var model: SuitabilityScoreModel

    fun setup(context: Context) {
        model = SuitabilityScoreModel(context)

        driversLiveData.postValue(model.shipmentsAssignedData)
    }
}