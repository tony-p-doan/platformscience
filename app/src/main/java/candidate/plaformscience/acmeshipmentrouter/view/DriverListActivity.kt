package candidate.plaformscience.acmeshipmentrouter.view

import android.content.DialogInterface
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import candidate.plaformscience.acmeshipmentrouter.R
import candidate.plaformscience.acmeshipmentrouter.util.DriversAdapter
import candidate.plaformscience.acmeshipmentrouter.viewmodel.DriverListViewModel


class DriverListActivity : AppCompatActivity() {
    private val viewModel: DriverListViewModel by viewModels()

    lateinit var driverAdapter: DriversAdapter
    lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driver_list)

        recyclerView = findViewById(R.id.rvDrivers)
        recyclerView.setHasFixedSize(true)

        val linearLayoutManager = LinearLayoutManager(this)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL

        recyclerView.layoutManager = linearLayoutManager

        viewModel.driversLiveData.observe(this, { drivers ->
            driverAdapter = DriversAdapter(drivers, this)

            driverAdapter.onItemClick = { routeData ->
                val dialogBuilder = AlertDialog.Builder(this)

                dialogBuilder.setMessage("The Suitability Score is: " + routeData.suitabilityScore + "\n\n" + "Street Address: " + routeData.shipment.streetAddress)
                    .setCancelable(false)
                    .setPositiveButton("OK", DialogInterface.OnClickListener {
                            dialog, id -> dialog.dismiss()
                    })

                val alert = dialogBuilder.create()

                alert.setTitle("Here's the shipping info for: " + routeData.driver.name)

                alert.show()
            }

            recyclerView.adapter = driverAdapter
        })

        viewModel.setup(this)
    }
}