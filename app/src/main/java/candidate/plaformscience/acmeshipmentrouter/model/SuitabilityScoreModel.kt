package candidate.plaformscience.acmeshipmentrouter.model

import android.R
import android.content.Context
import candidate.plaformscience.acmeshipmentrouter.util.SuitabilityScoreCalculator
import org.json.JSONObject
import java.io.*
import java.util.*


class SuitabilityScoreModel(val context: Context) {
    val shipmentRouterData: MutableList<RouteData> = mutableListOf<RouteData>()

    val shipmentsAssignedData: MutableList<RouteData> = mutableListOf<RouteData>()

    val driverData: ArrayList<Driver> = arrayListOf()
    val shipmentData: ArrayList<Shipment> = arrayListOf()

    val dataCalculator = SuitabilityScoreCalculator()

    val assignedDrivers: ArrayList<String> = arrayListOf()
    val assignedAddresses: ArrayList<String> = arrayListOf()

    init {
        readAndParseRawJSON()
        calculateSuitabilityScores()
        sortByDescendingScores()
        assignDriversByScores()
    }

    fun assignDriversByScores() {
        for (i in 0 .. shipmentRouterData.size - 1) {
            if (assignedDrivers.contains(shipmentRouterData.get(i).driver.name)) {
                continue
            }
            if (assignedAddresses.contains(shipmentRouterData.get(i).shipment.streetAddress)) {
                continue
            }
            shipmentsAssignedData.add(shipmentRouterData.get(i))
            assignedAddresses.add(shipmentRouterData.get(i).shipment.streetAddress)
            assignedDrivers.add(shipmentRouterData.get(i).driver.name)
        }
    }

    fun sortByDescendingScores() {
        shipmentRouterData.sortByDescending { it.suitabilityScore }
    }

    fun calculateSuitabilityScores() {
        for (i in 0 .. driverData.size - 1) {
            for (j in 0 .. shipmentData.size - 1) {
                val driver = driverData.get(i)
                val street = shipmentData.get(j)
                shipmentRouterData.add(RouteData(driver = Driver(driver.name), shipment = Shipment(streetAddress = street.streetAddress), suitabilityScore =
                    if (dataCalculator.isLengthEven(street.streetAddress)) dataCalculator.scaleSSValueByAlgorithmRule3(dataCalculator.getSSValueByAlgorithmRule1(driver.name), street.streetAddress, driver.name)
                    else dataCalculator.scaleSSValueByAlgorithmRule3(dataCalculator.getSSValueByAlgorithmRule2(driver.name), street.streetAddress, driver.name)))
            }
        }
    }

    fun readAndParseRawJSON() {
        val `is`: InputStream = context.getResources().openRawResource(
                context.getResources().getIdentifier("acme_data_file",
                        "raw", context.getPackageName()))
        val writer: Writer = StringWriter()
        val buffer = CharArray(1024)
        try {
            val reader: Reader = BufferedReader(InputStreamReader(`is`, "UTF-8"))
            var n: Int
            while (reader.read(buffer).also { n = it } != -1) {
                writer.write(buffer, 0, n)
            }
        } finally {
            `is`.close()
        }

        val jsonString: String = writer.toString()

        val jsonObj = JSONObject(jsonString)

        if (jsonObj.has("shipments")) {
            val shipmentsArr = jsonObj.getJSONArray("shipments")

            for (i in 0 .. shipmentsArr.length() - 1) {
                shipmentData.add(Shipment(shipmentsArr.getString(i)))
            }
        }

        if (jsonObj.has("drivers")) {
            val driversArr = jsonObj.getJSONArray("drivers")

            for (i in 0 .. driversArr.length() - 1) {
                driverData.add(Driver(driversArr.getString(i)))
            }
        }
    }
}