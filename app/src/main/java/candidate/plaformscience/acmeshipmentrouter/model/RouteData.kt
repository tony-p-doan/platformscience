package candidate.plaformscience.acmeshipmentrouter.model

class RouteData(
    val driver: Driver,
    val shipment: Shipment,
    var suitabilityScore: Double
)